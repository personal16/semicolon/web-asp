﻿using System;
using System.Collections.Generic;
using System.Text;
using DNTPersianUtils.Core;
using Semicolon.Common.Dto;
using Semicolon.Domain;

namespace Semicolon.Services
{
    public class PostDto:BaseDto<PostDto, Post>
    {
        public string Title { get; set; }
        public string Discretion { get; set; }
        public DateTime DateCreate { get; set; }
        public int UserId { get; set; }
        public string PersianDate => DateCreate.ToFriendlyPersianDateTextify();
     

    }
}
