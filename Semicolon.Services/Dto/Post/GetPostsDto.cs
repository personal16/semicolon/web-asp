﻿using DNTPersianUtils.Core;
using Semicolon.Common.Dto;
using Semicolon.Domain;
using Semicolon.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Semicolon.Services 
{
   public class GetPostsDto : BaseDto<GetPostsDto, Post>
    {
        public string Title { get; set; }
        public string Discretion { get; set; }
        public string UserName { get; set; }
        public int UserId { get; set; }
        public DateTime DateCreate { get; set; }
        public string PersianDate => DateCreate.ToFriendlyPersianDateTextify(false, true, true);
        public List<TagDto> Tags { get; set; }
    }
}
