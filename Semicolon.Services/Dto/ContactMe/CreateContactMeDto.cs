﻿using Semicolon.Common.Dto;
using Semicolon.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Semicolon.Services.Dto 
{
  public  class CreateContactMeDto:   BaseDto<CreateContactMeDto,ContactMe>
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
    }
}
