﻿using Semicolon.Common.Dto;
using Semicolon.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Semicolon.Services.Dto
{
  public  class GetCategoriesDto: BaseDto<GetCategoriesDto, Categories>
    {
        public string Name { get; set; }
     
        public int? CategoriesId { get; set; }
        public string Discretion { get; set; }


    }
}
