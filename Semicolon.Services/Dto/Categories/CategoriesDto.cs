﻿using System;
using System.Collections.Generic;
using System.Text;
using Semicolon.Common.Dto;
using Semicolon.Domain;
namespace Semicolon.Services.Dto 
{
  public  class CategoriesDto: BaseDto<CategoriesDto, Categories>
    {
        public string Name { get; set; }
 
        public int? CategoriesId { get; set; }
    }
}
