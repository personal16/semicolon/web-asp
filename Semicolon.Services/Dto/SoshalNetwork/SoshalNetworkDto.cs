﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Semicolon.Services.Dto.SoshalNetwork
{
   public class SoshalNetworkDto
    {
        public int UserId { get; set; }
     

        public string Name { get; set; }
        public string Icon { get; set; }
        public string Url { get; set; }
    }
}
