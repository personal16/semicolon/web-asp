﻿using Semicolon.Common.Dto;
using Semicolon.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Semicolon.Services.Dto 
{
   public class TagDto : BaseDto<TagDto, Tags>
    {
        public string Tags { get; set; }
    }
}
