﻿using AutoMapper;
using Semicolon.Common.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Semicolon.Domain;
namespace Semicolon.Services.Dto.User
{
   public class UserLogin : IHaveCustomMapping
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public void CreateMappings(Profile profile)
        {
            profile.CreateMap<Semicolon.Domain.User, UserLogin>().ReverseMap();
        }
    }
}
