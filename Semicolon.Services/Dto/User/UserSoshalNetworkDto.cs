﻿using Semicolon.Services.Dto.SoshalNetwork;
using System;
using System.Collections.Generic;
using System.Text;

namespace Semicolon.Core.Dto
{
 public   class UserDto
    {
        public string LName { get; set; }
        public string FName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string AboutMe { get; set; }
        public List<SoshalNetworkDto> SoshalNetwork { get; set; }
        public List<string> SliderImage { get; set; }
        public string Photo { get; set; }
    }
}
