﻿using Semicolon.Common.Dto;
using System.ComponentModel.DataAnnotations;

namespace Semicolon.Core.Dto.User
{
    public class RegisterUserDto:BaseDto<RegisterUserDto, Domain.User>
    {
        [Required(ErrorMessage ="فیلد {1} الزلمی است")]
        [Display(Name = "نام")]
        public string LName { get; set; }

        [Required(ErrorMessage = "فیلد {1} الزلمی است")]
        [Display(Name = "نام خانوادگی")]
        public string FName { get; set; }

        //[Required(ErrorMessage = "فیلد {1} الزلمی است")]
        //[Display(Name = "نام کاربری")]
        //[StringLength(maximumLength: 50,ErrorMessage = "نام کاربری باید بین 4 الی 50 کاراکتر باشد",MinimumLength = 4)]
        //public string UserName { get; set; }

        [Required(ErrorMessage = "فیلد {1} الزلمی است")]
        [Display(Name = "شماره تلفن")]
        [StringLength(maximumLength: 11,ErrorMessage = "شماره تلفن اشتباه است",MinimumLength = 11)]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "فیلد {1} الزلمی است")]
        [EmailAddress(ErrorMessage ="ایمیل اشتباه است")]
        [Display(Name ="ایمیل")]
        public string Email { get; set; }

        [Required(ErrorMessage = "فیلد {1} الزلمی است")]
        [Display(Name = "گذرواژه")]
        public string Password { get; set; }

        [Required(ErrorMessage = "فیلد {1} الزلمی است")]
        [Display(Name = "تکرار گذرواژه")]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }


    }
}
