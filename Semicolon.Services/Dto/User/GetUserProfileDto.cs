﻿using Semicolon.Core.Dto;
using Semicolon.Services.Dto;
using Semicolon.Services.Dto.SoshalNetwork;
using System;
using System.Collections.Generic;
using System.Text;

namespace Semicolon.Services 
{
  public  class GetUserProfileDto
    {
        
        public UserDto User { get; set; }
        public List<GetCategoriesDto> Categories { get; set; }
       public List<GetPostsDto> NewPosts { get; set; }
    }
}
