﻿using Semicolon.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Semicolon.Services 
{
 public   interface IContactMeService : IGenericDataSource<ContactMe>
    {
    }
}
