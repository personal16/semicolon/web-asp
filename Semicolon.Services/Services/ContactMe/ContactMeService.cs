﻿using Semicolon.DataLayer.Context;
using Semicolon.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Semicolon.Services 
{
   public class ContactMeService: GenericDataSource<ContactMe>, IContactMeService
    {
        public ContactMeService(IUnitOfWork uow):base(uow)
        {

        }
    }
}
