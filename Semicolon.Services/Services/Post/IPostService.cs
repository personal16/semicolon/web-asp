﻿using Semicolon.Domain;
using Semicolon.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Semicolon.Services 
{
 public   interface IPostService : IGenericDataSource<Post>
    {
         Task<GetPostsDto> GetPoetAsyncById(int id);
        Task<List<GetPostsDto>> GetPostsByCategoriesIdAsync(int id);
        Task<List<GetPostsDto>> GetPostsBytagAsync(string tags);
        Task<List<GetPostsDto>> GetNewPoetsAsync();
    }
}
