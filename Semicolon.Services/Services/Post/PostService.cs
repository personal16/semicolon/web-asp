﻿using Semicolon.DataLayer.Context;
using Semicolon.Domain;
using Semicolon.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Semicolon.Common;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Semicolon.Common.Extension;
namespace Semicolon.Services
{
    public class PostService : GenericDataSource<Post>,IPostService
    {
        private  DbSet<User> _user;
      
        public PostService(IUnitOfWork uow):base(uow)
        {
            _user = _uow.Set<User>();
        }

        public async Task<GetPostsDto> GetPoetAsyncById(int id)
        {

            return await  TEntity.Where(x => x.Id == id).Include(x => x.User).Include(x=> x.Tags).Select(x=> new GetPostsDto()
            {
                 Id=x.Id,
                 DateCreate=x.DateCreate,
                 Discretion=x.Discretion,
                 Title=x.Title,
                 UserId=x.UserId,
                 UserName=x.User.UserName ,
                 Tags=x.Tags.Select(t=> new TagDto() { 
                 Tags=t.Tag
                 }).ToList()
            
            }).SingleOrDefaultAsync();
        }

        public async  Task<List<GetPostsDto>> GetNewPoetsAsync()
        {
          
            return await TEntity.Include(x => x.User).Select(x => new GetPostsDto()
            {
                Id = x.Id,
                DateCreate = x.DateCreate,
                Discretion = x.Discretion.Substring(0, x.Discretion.Length > 100 ? 100 : x.Discretion.Length).ToString().StripHTML()+ "...",
                Title = x.Title ,
                UserId = x.UserId,
                UserName = x.User.UserName

            }).OrderBy(x=> x.DateCreate).Take(10).ToListAsync();
        }

        public async Task<List<GetPostsDto>> GetPostsByCategoriesIdAsync(int id)
        {
            return await TEntity.Where(x=> x.CategoriesId==id).Include(x => x.User).Include(x=> x.Tags).Select(x => new GetPostsDto()
            {
                Id = x.Id,
                DateCreate = x.DateCreate,
                Discretion = x.Discretion.Substring(0, x.Discretion.Length > 100 ? 100 : x.Discretion.Length).ToString().StripHTML(),
                Title = x.Title,
                UserId = x.UserId,
                UserName = x.User.UserName,
                Tags=x.Tags.Select(j=> new TagDto() { 
                Tags=j.Tag
                }).ToList()

            }).ToListAsync();
        }

        public async Task<List<GetPostsDto>> GetPostsBytagAsync(string tags)
        {
             
            return await TEntity.Include(x=> x.Tags ).Where(x=> x.Tags.Any(t=> t.Tag.Equals(tags))).Select(x => new GetPostsDto() {
                Id = x.Id,
                DateCreate = x.DateCreate,
                Discretion = x.Discretion.Substring(0, x.Discretion.Length > 100 ? 100 : x.Discretion.Length).ToString().StripHTML(),
                Title = x.Title,
                UserId = x.UserId,
                UserName = x.User.UserName,
                Tags = x.Tags.Select(j => new TagDto()
                {
                    Tags = j.Tag
                }).ToList()


            }).ToListAsync();
        }
    }
}
