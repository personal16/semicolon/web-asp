﻿using Microsoft.EntityFrameworkCore;
using Semicolon.DataLayer.Context;
using Semicolon.Domain;
using Semicolon.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Semicolon.Services
{
 public   class CategoriesService : GenericDataSource<Categories>, ICategoriesService
    {
        private readonly DbSet<Post> _post;
       

        public CategoriesService(IUnitOfWork uow) : base(uow)
        {
            _post = _uow.Set<Post>();

        }

        public async Task<List<GetCategoriesDto>> GetCategoriesAsync(int userid)
        {
         
           return  await  (from p in _post
                           join c in  TEntity on p.CategoriesId equals c.Id into Details
                           from m in Details.DefaultIfEmpty()
                           where (m == null || (m != null)) && m.UserId==userid
                           select new   GetCategoriesDto()
                         {
                             CategoriesId = m.CategoriesId,
                             Discretion = m.Discretion,
                             Id = m.Id,
                             Name = m.Name
                         }).ToListAsync();

            
           


        }
    }
}
