﻿using Semicolon.Domain;
using Semicolon.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Semicolon.Services
{
   public interface ICategoriesService : IGenericDataSource<Categories>
    {
        Task<List<GetCategoriesDto>> GetCategoriesAsync(int userid);
    }
}
