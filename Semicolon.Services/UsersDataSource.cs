using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Semicolon.Core.Dto;
using Semicolon.Core.HardCode;
using Semicolon.DataLayer.Context;
using Semicolon.Domain;
using Semicolon.Services.Dto;
using Semicolon.Services.Dto.SoshalNetwork;
using Semicolon.Services.Dto.User;
using Semicolon.Services.jwt;
using Semicolon.Common.Extension;
namespace Semicolon.Services
{
 public interface IUsersDataSource : IGenericDataSource<User>
    {
        User FindUser(string email, string password);
        Task<User> FindUserAsync(string email);
        Task<GetUserProfileDto> GetProfile(int userid);
    }

    public class UsersDataSource : GenericDataSource<User>, IUsersDataSource
    {
        private readonly DbSet<Post> _post;
        private readonly ISecurityService _securityService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly DbSet<Categories> _categories;
        private readonly IOptionsSnapshot<PathSettings> _patchSettings;
        public string Domain { get; set; }
        public UsersDataSource(
            ISecurityService securityService
            ,IUnitOfWork uow
            , IOptionsSnapshot<PathSettings> patchSettings
            , IHttpContextAccessor httpContextAccessor) :base(uow)
        {
            _securityService = securityService;
          
            _post = _uow.Set<Post>();
            _categories = _uow.Set<Categories>();
            _patchSettings = patchSettings;
            _httpContextAccessor = httpContextAccessor;
            Domain = $"{_httpContextAccessor.HttpContext.Request.Scheme}://{_httpContextAccessor.HttpContext.Request.Host}";
        }

        public User FindUser(string email, string password)
        {
            var hashedPassword = _securityService.GetSha256Hash(password);
            return base.FindData(user => user.Email.Equals(email, StringComparison.OrdinalIgnoreCase) && user.Password == hashedPassword);
        }

        public override Task<bool> AddData(User data, Func<User, bool> action)
        {
            return base.AddData(data, user =>
            {
                var isEmailUnique = base.FindData(x => x.Email.Equals(user.Email, StringComparison.OrdinalIgnoreCase)) == null;
                if (!isEmailUnique)
                {
                    return false;
                }
                user.Password = _securityService.GetSha256Hash(user.Password);
                return true;
            });
        }

        public override bool UpdateData(int id, User data, Func<User, bool> action = null)
        {
            return base.UpdateData(id, data, user =>
           {
               user.Password = _securityService.GetSha256Hash(user.Password);
               return true;
           });
        }

        protected override List<User> SeedDataSource()
        {
            return new List<User>{
                new User
                {
                    Id = 1,
                    UserName = "Vahid N.",
                    Email = "vahid@dotnettips.info",
                    Password = _securityService.GetSha256Hash("12345"),
                  
                }
            };
        }

        public async Task<User> FindUserAsync(string email)
        {
            return  await TEntity.Where(x => x.Email == email).SingleOrDefaultAsync();
        }

        public async Task<GetUserProfileDto> GetProfile(int userid)
        {
           

            //TODO MustBe Clean
            return await TEntity.Where(x => x.Id == userid).Select(x => new GetUserProfileDto() {
                User = new UserDto()
                {
                    AboutMe = x.AboutMe,
                    Email = x.Email,
                    FName = x.FName,
                     LName = x.LName,
                    UserName = x.UserName,
                    Photo= Domain+_patchSettings.Value.ProfileImage+x.Photo,
                    SoshalNetwork = x.SoshalNetwork.Select(c => new SoshalNetworkDto()
                    {
                        Icon = c.Icon,
                        Name = c.Name,
                        Url = c.Url,
                        UserId = c.UserId
                    }).ToList(),
                    SliderImage =  x.SliderImage.Select(x => Domain + _patchSettings.Value.SliderImage + x.Photo).ToList(),
                },
            NewPosts= _post.Where(x=> x.UserId==userid).Include(x => x.User).Select(x => new GetPostsDto()
            {
                Id = x.Id,
                DateCreate = x.DateCreate,
                Discretion = x.Discretion.Substring(0, x.Discretion.Length > 100 ? 100 : x.Discretion.Length).ToString().StripHTML() + "...",
                Title = x.Title,
                UserId = x.UserId,
                UserName = x.User.UserName

            }).OrderBy(x => x.DateCreate).Take(10).ToList(),

            Categories = (from p in _post
                           join c in _categories on p.CategoriesId equals c.Id into Details
                           from m in Details.DefaultIfEmpty()
                           where (m == null || (m != null)) && m.UserId == userid
                           select new GetCategoriesDto()
                           {
                               CategoriesId = m.CategoriesId,
                               Discretion = m.Discretion,
                               Id = m.Id,
                               Name = m.Name
                           }).ToList() 
        }).FirstOrDefaultAsync();
        }
    }
}