﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Semicolon.Core.HardCode
{
    public class PathSettings
    {
        public string ProfileImage { get; set; }
        public string SliderImage { get; set; }
    }
}
