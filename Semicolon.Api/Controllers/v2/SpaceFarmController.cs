using Microsoft.AspNetCore.Mvc;

namespace Semicolon.Api.Controllers.v2
{
    [ApiVersion("2")]
[Route("api/v{version:apiVersion}/[controller]")]
[ApiController]
public class SpaceFarmController : ControllerBase
{
    [HttpGet("Potatoes")]
    public string SpacePotatoes() => "Space Potatoes v1";
}
}