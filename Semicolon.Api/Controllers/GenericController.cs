using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Semicolon.Api.Model;
using Semicolon.Api.Model.ApiResult;
using Semicolon.Api.Model.Filter;
using Semicolon.Common;
using Semicolon.Common.Dto;
using Semicolon.Domain;
using Semicolon.Services;


namespace Semicolon.Api.Controllers
{
    //[Authorize]
    [ApiController]
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [IgnoreAntiforgeryToken]
    [ApiResultFilterAttribute]
    public class GenericController<TEntity, TDto> : ControllerBase
       where TEntity : BaseEntity, new()
       where TDto : BaseDto<TDto, TEntity>, new()

    {
        protected readonly IGenericDataSource<TEntity> DataSource;
        private readonly IMapper _mapper;
        public GenericController(IGenericDataSource<TEntity> dataSource, IMapper mapper)
        {
            DataSource = dataSource;
            _mapper = mapper;
        }

        [HttpGet]
        public async virtual Task<ApiResult<List<TDto>>> Get()
        {
            var data= await DataSource.GetAllData();
            var dto = _mapper.Map<List<TDto>>(data);
            return Ok(dto);
        }

        [HttpGet("{id}")]
        public async virtual Task<ApiResult<TDto>> Get(int id)
        {
            var data =await DataSource.FindData(id);
            if (data == null)
            {
                return NotFound();
            }
            var dtodata = _mapper.Map<TDto>(data);
            return Ok(dtodata);
        }

        [HttpDelete("{id}")]
        public virtual ApiResult Delete(int id)
        {
            var deleted = DataSource.DeleteData(id);
            if (deleted)
            {
                return Ok();
            }
            return NotFound();
        }

        [HttpPost()]
        public virtual async Task<ApiResult<TDto>> Create([FromBody]TDto data)
        {
            //ToDO
            var entitydata = data.ToEntity(_mapper);
            var result = await DataSource.AddData(entitydata);
            if (!result)
            {
                return BadRequest();
            }
            var datadto = _mapper.Map<TDto>(entitydata);
            return CreatedAtAction(nameof(Create), new { datadto.Id }, datadto);


        }

        [HttpPut("{id}")]
        public virtual ApiResult<TDto> Update(int id, [FromBody]TDto data)
        {
            var dataentity = data.ToEntity(_mapper);
            var updated = DataSource.UpdateData(id, dataentity);
            if (updated)
            {
                return Ok(data);
            }
            return NotFound();
        }
    }
}