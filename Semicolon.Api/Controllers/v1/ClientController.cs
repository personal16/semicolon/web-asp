﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Semicolon.Api.Model.ApiResult;
using Semicolon.Services.Dto;

namespace Semicolon.Api.Controllers.v1
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]

    public class ClientController : ControllerBase
    {
        private readonly ILogger<ClientController> _logger;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ClientController(IHttpContextAccessor httpContextAccessor, ILogger<ClientController> logger)
        {
            _httpContextAccessor = httpContextAccessor;
            _logger = logger;
            
        }

        [HttpGet("[action]")]
        public async Task<ApiResult<SliderDto>> getSliderImages() {
          
            Random r = new Random();
            var result =new SliderDto();
            var filePathsprogramming = Directory.GetFiles(@"wwwroot\SliderImages\programming").ToList();
            result.Programming = (filePathsprogramming[r.Next(0, filePathsprogramming.Count)]).Replace("wwwroot\\", "").Replace("\\", "/");
            var filePathsMe = Directory.GetFiles(@"wwwroot\SliderImages\Me").ToList();
            result.Me = (filePathsMe[r.Next(0, filePathsMe.Count)]).Replace("wwwroot\\", "").Replace("\\", "/");
            return Ok(result);
        }
    }
}