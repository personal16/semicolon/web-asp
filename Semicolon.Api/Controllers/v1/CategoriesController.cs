﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Semicolon.Api.Model.ApiResult;
using Semicolon.Api.Model.Filter;
using Semicolon.Domain;
using Semicolon.Services;
using Semicolon.Services.Dto;
using Semicolon.Services.jwt;

namespace Semicolon.Api.Controllers.v1
{
    [ApiController]
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [IgnoreAntiforgeryToken]
    [ApiResultFilter]
    public class CategoriesController :   ControllerBase
    {
        private readonly ICategoriesService _categoriesService;
        private readonly IUsersService _usersService;
        private readonly IMapper _mapper;
        public CategoriesController(
            ICategoriesService dataSource,
            IMapper mapper,
           IUsersService usersService)  
        {

            _mapper = mapper;
            _categoriesService = dataSource;
            _usersService = usersService;
        }



        [HttpGet("[action]/{username}")]
        public async   Task<ApiResult<List<GetCategoriesDto>>> GetCategories(string username)
        {
            var user = await   _usersService.FindUserAsync(username: username);
            if (user==null)
                return NotFound("UserNotFound");
            var data = await _categoriesService.GetCategoriesAsync(user.Id);
            //var dto = _mapper.Map<List<GetPostsDto>>(data);
            return Ok(data);
        }
    }
}