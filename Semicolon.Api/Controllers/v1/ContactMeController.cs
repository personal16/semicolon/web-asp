﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Semicolon.Domain;
using Semicolon.Services;
using Semicolon.Services.Dto;

namespace Semicolon.Api.Controllers.v1
{

    public class ContactMeController : GenericController<ContactMe, CreateContactMeDto>
    {

        private readonly IMapper _mapper;
        private readonly IContactMeService _contactMeService;
        public ContactMeController(
            IContactMeService dataSource,
            IMapper mapper) : base(dataSource, mapper)
        {

            _mapper = mapper;
            _contactMeService = dataSource;
        }
    }
}