using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Semicolon.Api.Model.ApiResult;
using Semicolon.Api.Model.Filter;
using Semicolon.Core.Dto;
using Semicolon.Core.Dto.User;
using Semicolon.Domain;
using Semicolon.Services;
using Semicolon.Services.Dto;
using Semicolon.Services.Dto.User;
using Semicolon.Services.jwt;
using System;
using System.Threading.Tasks;

namespace Semicolon.Api.Controllers.v1
{
    [ApiController]
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [IgnoreAntiforgeryToken]
    [ApiResultFilter]

    public class UsersController : ControllerBase
    {
        private readonly ICategoriesService _categoriesService;
        private readonly IUsersService _usersService;
        private readonly IMapper _mapper;
        private readonly IUsersDataSource _dataSource;
        public UsersController(
            IUsersDataSource dataSource,
            IMapper mapper,
            ICategoriesService categoriesService,
           IUsersService usersService)
        {
            _categoriesService = categoriesService;
            _mapper = mapper;
            _dataSource = dataSource;
            _usersService = usersService;
        }


        [HttpPost("[action]")]
        public async Task<ApiResult<User>> RegisterUser([FromBody] RegisterUserDto data)
        {
            var user = await _dataSource.FindUserAsync(data.Email);
            if (user != null)
                return BadRequest("????? ?????? ??? ! ");

            return await _usersService.CreateAccount(data);
        }

        [HttpGet("[action]/{username}")]
        public async Task<ApiResult<GetUserProfileDto>> getProfile(string username)
        {
            var userprofile = new GetUserProfileDto();
            var user = await _usersService.FindUserAsync(username: username.ToLower());
            if (user == null)
                return NotFound("UserNotFound");
            userprofile = await _dataSource.GetProfile(user.Id);

            return Ok(userprofile);
        }
    }
}