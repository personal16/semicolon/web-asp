using Microsoft.AspNetCore.Mvc;

namespace Semicolon.Api.Controllers.v1
{
 
[ApiController]
public class SpaceFarmController : ControllerBase
{
    [HttpGet("Potatoes")]
    public string SpacePotatoes() => "Space Potatoes v1";
}
}