 
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Semicolon.Services.jwt;

namespace Semicolon.Api.Controllers.v1.jwt
{
 [ApiVersion("1")] 
   [Route("v{version:apiVersion}/[controller]")] 
     
    [Authorize(Policy = CustomRoles.Editor)]
    public class MyProtectedEditorsApiController : ControllerBase
    {
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(new
            {
                Id = 1,
                Title = "Hello from My Protected Editors Controller! [Authorize(Policy = CustomRoles.Editor)]",
                Username = this.User.Identity.Name
            });
        }
    }
}