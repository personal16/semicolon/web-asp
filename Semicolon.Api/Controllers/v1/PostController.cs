﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Semicolon.Api.Model.ApiResult;
using Semicolon.Domain;
using Semicolon.Services;
using Semicolon.Services.Dto;
using Semicolon.Services.jwt;

namespace Semicolon.Api.Controllers.v1
{
  
    public class PostController :   GenericController<Post, PostDto>
    {
     
        private readonly IMapper _mapper;
        private readonly IPostService _postService;
        public PostController(
            IPostService dataSource,
            IMapper mapper) : base(dataSource, mapper)
        {
            
            _mapper = mapper;
            _postService = dataSource;
        }

        [HttpGet("[action]/{id:int}")]
        public async     Task<ApiResult<GetPostsDto>> GetPostsById(int id)
        {
            var data = await _postService.GetPoetAsyncById(id);
            //var dto = _mapper.Map<List<GetPostsDto>>(data);
            return Ok(data);
        }
        [HttpGet("[action]/{id:int}")]
        public async   Task<ApiResult<List<GetPostsDto>>> GetPostsByCategoriesId(int id)
        {
            var data = await _postService.GetPostsByCategoriesIdAsync(id);
            //var dto = _mapper.Map<List<GetPostsDto>>(data);
            return Ok(data);
        }
        [HttpGet("[action]")]
        public async   Task<ApiResult<List<GetPostsDto>>> GetNewPosts()
        {
            var data = await _postService.GetNewPoetsAsync();
            //var dto = _mapper.Map<List<GetPostsDto>>(data);
            return Ok(data);
        }

        [HttpGet("[action]/{tag}")]
        public async Task<ApiResult<List<GetPostsDto>>> GetPostsBytag(string tag)
        {
            var data = await _postService.GetPostsBytagAsync(tag);
            //var dto = _mapper.Map<List<GetPostsDto>>(data);
            return Ok(data);
        }
    }
}