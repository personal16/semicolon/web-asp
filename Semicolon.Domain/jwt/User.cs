﻿using Semicolon.Common.Dto;
using System;
using System.Collections.Generic;
 
namespace Semicolon.Domain
{
    public class User: BaseEntity
    {
        public User()
        {
            UserRoles = new HashSet<UserRole>();
            UserTokens = new HashSet<UserToken>();
        }
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public string LName { get; set; }
        public string FName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string AboutMe { get; set; }
        public DateTimeOffset? LastLoggedIn { get; set; }

        public string Photo { get; set; }
        /// <summary>
        /// every time the user changes his Password,
        /// or an admin changes his Roles or stat/IsActive,
        /// create a new `SerialNumber` GUID and store it in the DB.
        /// </summary>
        public string SerialNumber { get; set; }

        public virtual ICollection<UserRole> UserRoles { get; set; }

        public virtual ICollection<UserToken> UserTokens { get; set; }
        public virtual ICollection<Post> Post { get; set; }    
        public virtual ICollection<SoshalNetwork> SoshalNetwork { get; set; }
        public ICollection<Categories>  Categories { get; set; }
        public ICollection<SliderImage> SliderImage { get; set; }
    }
}
