using Semicolon.Common.Dto;
using System.Collections.Generic;

namespace Semicolon.Domain
{
    public class Role : BaseEntity
    {
        public Role()
        {
            UserRoles = new HashSet<UserRole>();
        }

   
        public string Name { get; set; }

        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}