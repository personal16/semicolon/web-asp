﻿using Semicolon.Common.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Semicolon.Domain
{
  public  class Post:BaseEntity
    {
        public string Title { get; set; }
        public string Discretion { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }
        public int Lilke { get; set; }
        public Categories Categories { get; set; }
        public int CategoriesId { get; set; }

        public ICollection<Tags> Tags { get; set; }
    }
}
