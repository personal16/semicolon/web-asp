﻿using Semicolon.Common.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Semicolon.Domain
{
  public  class SoshalNetwork : BaseEntity
    {
        public int UserId { get; set; }
        public virtual User User { get; set; }

        public string Name { get; set; }
        public string Icon { get; set; }
        public string Url { get; set; }
    }
}