﻿using Semicolon.Common.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Semicolon.Domain
{
  
      public class Categories:BaseEntity
    {
        public string Name { get; set; }
        public Categories Categorie { get; set; }
        public int? CategoriesId { get; set; }
        public string Discretion { get; set; }
        public ICollection<Categories> CategoriesParent { get; set; }
        public ICollection<Post> Post { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }

    }
}
