﻿using Semicolon.Common.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Semicolon.Domain
{
  public      class Tags : BaseEntity
    {
        public string Tag { get; set; }
        public Post Post { get; set; }
        public int PostId { get; set; }
    }
}
