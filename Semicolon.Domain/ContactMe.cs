﻿using Semicolon.Common.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Semicolon.Domain
{
   public class ContactMe : BaseEntity
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
    }
}
