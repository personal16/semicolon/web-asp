﻿using Semicolon.Common.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Semicolon.Domain
{
   public class SliderImage:BaseEntity
    {
        public string Photo { get; set; }
        public User   User { get; set; }
        public int UserId { get; set; }
    }
}
