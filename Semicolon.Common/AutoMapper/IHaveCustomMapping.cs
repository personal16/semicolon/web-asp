﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Semicolon.Common.AutoMapper
{
    public interface IHaveCustomMapping
    {
        void CreateMappings(Profile profile);
    }
}
