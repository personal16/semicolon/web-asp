﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Semicolon.Common.Dto
{
    public interface IEntity
    {
    }

    public abstract class BaseEntity<TKey> : IEntity
    {
        public TKey Id { get; set; }
        
        public bool IsDelete { get; set; }
        public DateTime DateCreate { get; set; }
        public BaseEntity()
        {
            DateCreate = DateTime.Now;
        }
    }

    public abstract class BaseEntity : BaseEntity<int>
    {
    }
}
