using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Semicolon.DataLayer.Context
{
    public class ApplicationDbContextFactory : IDesignTimeDbContextFactory<ApplicationContext>
    {
        public ApplicationContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ApplicationContext>();
            builder.UseSqlServer("Data Source=.;Initial Catalog=Semicolon_db;Integrated Security=True;User ID=semicolon;Password=Sm5688292",
                optionsBuilder => optionsBuilder.MigrationsAssembly(typeof(ApplicationContext).GetTypeInfo().Assembly.GetName().Name));

            return new ApplicationContext(builder.Options);
        }
    }
}