﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Semicolon.Domain;
using System;
using System.Collections.Generic;
using System.Text;
namespace Semicolon.DataLayer.Configuration
{
   public class CategoriesConfiguration : IEntityTypeConfiguration<Categories>
    {
        public void Configure(EntityTypeBuilder<Categories> builder)
        {
            builder.HasMany(x => x.CategoriesParent)
                .WithOne(x => x.Categorie)
                .HasForeignKey(x => x.CategoriesId);
          


        }
    }
}

