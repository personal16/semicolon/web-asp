﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Semicolon.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Semicolon.DataLayer.Configuration
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasMany(x => x.Post).
                WithOne(x => x.User).
                HasForeignKey(x => x.UserId).
                OnDelete(DeleteBehavior.Cascade);

            builder.HasMany(x => x.SoshalNetwork).
                WithOne(x => x.User).
                HasForeignKey(x => x.UserId).
                OnDelete( DeleteBehavior.Cascade);

            builder.HasMany(x => x.Categories).
             WithOne(x => x.User).
             HasForeignKey(x => x.UserId).
            OnDelete(DeleteBehavior.Restrict);


            builder.HasMany(x => x.SliderImage).
           WithOne(x => x.User).
           HasForeignKey(x => x.UserId).
         OnDelete(DeleteBehavior.Cascade);
        }
    }
}
