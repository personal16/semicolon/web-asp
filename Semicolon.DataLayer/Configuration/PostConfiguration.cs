﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Semicolon.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Semicolon.DataLayer.Configuration
{
    class PostConfiguration : IEntityTypeConfiguration<Post>
    {
        public void Configure(EntityTypeBuilder<Post> builder)
        {
            builder.HasOne(x => x.Categories)
                .WithMany(x=> x.Post).
                HasForeignKey(x=> x.CategoriesId).
                OnDelete(DeleteBehavior.Cascade);

            builder.HasMany(x => x.Tags)
            .WithOne(x => x.Post).
            HasForeignKey(x => x.PostId).
            OnDelete(DeleteBehavior.Cascade);
        }
    }
}