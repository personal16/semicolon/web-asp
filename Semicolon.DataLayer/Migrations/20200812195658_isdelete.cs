﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Semicolon.DataLayer.Migrations
{
    public partial class isdelete : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IaDelete",
                table: "UserTokens");

            migrationBuilder.DropColumn(
                name: "IaDelete",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "IaDelete",
                table: "UserRoles");

            migrationBuilder.DropColumn(
                name: "IaDelete",
                table: "Tags");

            migrationBuilder.DropColumn(
                name: "IaDelete",
                table: "SoshalNetwork");

            migrationBuilder.DropColumn(
                name: "IaDelete",
                table: "SliderImage");

            migrationBuilder.DropColumn(
                name: "IaDelete",
                table: "Roles");

            migrationBuilder.DropColumn(
                name: "IaDelete",
                table: "Post");

            migrationBuilder.DropColumn(
                name: "IaDelete",
                table: "ContactMe");

            migrationBuilder.DropColumn(
                name: "IaDelete",
                table: "Categories");

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "UserTokens",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "Users",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "UserRoles",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "Tags",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "SoshalNetwork",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "SliderImage",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "Roles",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "Post",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "ContactMe",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "Categories",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "UserTokens");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "UserRoles");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "Tags");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "SoshalNetwork");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "SliderImage");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "Roles");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "Post");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "ContactMe");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "Categories");

            migrationBuilder.AddColumn<bool>(
                name: "IaDelete",
                table: "UserTokens",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IaDelete",
                table: "Users",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IaDelete",
                table: "UserRoles",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IaDelete",
                table: "Tags",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IaDelete",
                table: "SoshalNetwork",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IaDelete",
                table: "SliderImage",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IaDelete",
                table: "Roles",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IaDelete",
                table: "Post",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IaDelete",
                table: "ContactMe",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IaDelete",
                table: "Categories",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
